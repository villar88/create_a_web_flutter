import "package:flutter/material.dart";
import "package:english_words/english_words.dart";
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "List Countries with Conoravirus",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.green,
        ),
        home: RandonWords());
  }
}

class RandonWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RandomWordsState();
  }
}

class User {
  var countryregion;
  var confirmed;
  var deaths;
  var recovered;

  User(this.countryregion, this.confirmed, this.deaths, this.recovered);

  User.fromJson(Map<String, dynamic> json) {
    countryregion = json['countryregion'];
    confirmed = json['confirmed'];
    deaths = json['deaths'];
    recovered = json['recovered'];
  }
}

class RandomWordsState extends State<RandonWords> {
  final _suggestion = <WordPair>[];
  final _save = Set<WordPair>();
  final _biggerFont = const TextStyle(fontSize: 20.0);
    List<User> _user = List<User>();

  Future<List<User>> _getUser() async {
    var data = await http.get(
        "https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/latest?onlyCountries=true");
    var jsonData = json.decode(data.body);
    // List<User> users = [];
    var users = List<User>();

    for (var u in jsonData) {
      User user =User(u["countryregion"], u['confirmed'], u['deaths'], u['recovered']);
      users.add(user);
    }
    // print(users.length);
    return users;
  }
  @override
  void initState() {
    _getUser().then((value) {
      setState(() {
        _user.addAll(value);
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          //header
          title: Text("List Countries with coronavirus"),
          // actions: <Widget>[
          //   IconButton(
          //     icon: Icon(Icons.list),
          //     onPressed: _pushSaved,
          //   ),
          // ],
        ),
        body: _buildCountries(),
        // body: _buildSuggestion(),
      );
  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        final listTile = _save.map((pair) {
          return ListTile(
            title: Text(
              pair.asCamelCase,
              style: _biggerFont,
            ),
          );
        });
        final divided =
            ListTile.divideTiles(tiles: listTile, context: context).toList();
        return Scaffold(
          appBar: AppBar(
            title: Text("More information"),
          ),
          body: ListView(
            children: divided,
          ),
        );
      }),
    );
  }

  Widget _buildSuggestion() {
    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) {
          return Divider(color: Colors.black);
        }
        if (i >= _suggestion.length) {
          _suggestion.addAll(generateWordPairs().take(10));
        }
        final index = i ~/ 2;
        return _buildRow(_suggestion[index]);
      },
    );
  }

  Widget _buildCountries() {
    return ListView.builder(
      itemBuilder: (context, index){
        return Card(
          child: Padding(
            padding: EdgeInsets.only(top:10.0, bottom: 10,left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  _user[index].countryregion,
                ),
                Text(
                  "Confirmed cases: "+_user[index].confirmed.toString()
                ),
                Text(
                  "Death cases: "+_user[index].deaths.toString()
                ),
                Text(
                  "Recovered cases: "+_user[index].recovered.toString()
                ),
              ],
            ),
          ),
        );
      },
      itemCount: _user.length,

    );
  }

  Widget _buildRow(WordPair pair) {
    final _alreadySave = _save.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        _alreadySave ? Icons.favorite : Icons.favorite_border,
        color: _alreadySave ? Colors.red : Colors.red,
      ),
      onTap: () {
        setState(() {
          if (_alreadySave) {
            _save.remove(pair);
          } else {
            _save.add(pair);
          }
          _pushSaved();
        });
      },
    );
  }

  //////////////////
  Widget _buildRowCountries(pairs) {
    final _alreadySave = _save.contains(pairs);
    var pairs2 = pairs;
    return ListTile(
      title: Text(
        pairs2,
        style: _biggerFont,
      ),
      trailing: Icon(
        _alreadySave ? Icons.favorite : Icons.favorite_border,
        color: _alreadySave ? Colors.red : Colors.red,
      ),
      onTap: () {
        setState(() {
          if (_alreadySave) {
            _save.remove(pairs2);
          } else {
            _save.add(pairs2);
          }
          // _pushSaved();
        });
      },
    );
  }
}
