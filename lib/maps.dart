import 'package:flutter/material.dart';
import 'package:google_maps/google_maps.dart';
import 'dart:ui' as ui;

class GoogleMap extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    String htmlId = "htmls";

    ui.platformViewRegistry.registerViewFactory(htmlId, (int viewId) {
      return HtmlElementView(viewType: htmlId);
    }
  }
}